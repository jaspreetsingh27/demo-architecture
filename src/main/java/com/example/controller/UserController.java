package com.example.controller;

import javax.annotation.Resource;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.domain.User;
import com.example.exception.UserAlreadyExistsException;
import com.example.exception.UserNotFoundException;
import com.example.service.UserService;
import com.example.util.Constants;
import com.example.util.RestResponse;
import com.example.util.RestUtils;


@RestController
@RequestMapping(value = Constants.API_PREFIX + "/auth")
public class UserController {

  @Resource
  private UserService userService;

  @RequestMapping(value = "/signup", method = RequestMethod.POST)
  @ResponseBody
  public ResponseEntity<RestResponse<User>> signup(@RequestBody User user)
      throws UserAlreadyExistsException {
    return RestUtils.successResponse(userService.signup(user), HttpStatus.CREATED);
  }

  @RequestMapping(value = "/login", method = RequestMethod.POST)
  @ResponseBody
  public ResponseEntity<RestResponse<User>> login(@RequestBody User user)
      throws UserNotFoundException {
    return RestUtils.successResponse(userService.login(user.getEmail(), user.getPassword()),
        HttpStatus.OK);
  }
}
