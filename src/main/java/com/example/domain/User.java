package com.example.domain;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class User extends AbstractMongoEntity {

  private static final long serialVersionUID = 681667438563041633L;

  @Indexed(unique = true)
  private String email;

  private String password;

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public User() {
    super();
  }

  public User(String email, String password) {
    super();
    this.email = email;
    this.password = password;
  }

  @Override
  public String toString() {
    return "User [email=" + email + ", password=" + password + "]";
  }
}
