package com.example.exception;

public class UserAlreadyExistsException extends Exception {

  private static final long serialVersionUID = -6898952360456198311L;

  private static final String DEFAULT_MESSAGE = "User Already Exist!";

  public UserAlreadyExistsException() {
    super(DEFAULT_MESSAGE);
  }

  public UserAlreadyExistsException(String message) {
    super(message);
  }

}
