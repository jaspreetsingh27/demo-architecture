package com.example.exception;

public class UserNotFoundException extends Exception {

  private static final long serialVersionUID = 5869271151408022264L;

  private static final String DEFAULT_MESSAGE = "User not found!";

  public UserNotFoundException() {
    super(DEFAULT_MESSAGE);
  }

  public UserNotFoundException(String message) {
    super(message);
  }

}
