package com.example.service;

import com.example.domain.User;
import com.example.exception.UserAlreadyExistsException;
import com.example.exception.UserNotFoundException;

public interface UserService {

  /**
   * This method should save a user into database.
   * 
   * @param user object to be saved in database.
   * @return the saved user object.
   */
  User save(User user);

  User findOne(String id) throws UserNotFoundException;

  /**
   * Should register a user into the database.
   * 
   * @param user object to be saved in database.
   * @returns the newly registered user.
   * @throws UserAlreadyExistsException If user email already exists in the database.
   * @throws UserAlreadyExistsException
   */
  User signup(User user) throws UserAlreadyExistsException;

  /**
   * This method allows the user to login.
   * 
   * @param email
   * @param password
   * @returns the user if the user enters the correct credentials.
   * @throws UserNotFoundException if the credentials do not match.
   */
  User login(String email, String password) throws UserNotFoundException;
}
