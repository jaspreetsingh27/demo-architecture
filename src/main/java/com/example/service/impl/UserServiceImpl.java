package com.example.service.impl;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.example.domain.User;
import com.example.exception.UserAlreadyExistsException;
import com.example.exception.UserNotFoundException;
import com.example.repository.UserRepository;
import com.example.service.UserService;

@Service
public class UserServiceImpl implements UserService {

  @Resource
  private UserRepository userRepository;

  @Override
  public User save(User user) {
    return userRepository.save(user);
  }

  @Override
  public User findOne(String id) throws UserNotFoundException {
    User user = userRepository.findOne(id);
    if (user == null) {
      throw new UserNotFoundException("Cannot find the User");
    }
    return user;
  }

  @Override
  public User signup(User user) throws UserAlreadyExistsException {
    User validateUser = userRepository.findByEmail(user.getEmail());

    if (validateUser != null) {
      throw new UserAlreadyExistsException(
          "Email address is already registered. Choose a different email.");
    }
    User userSave = new User();
    userSave.setEmail(user.getEmail());
    userSave.setPassword(user.getPassword());
    return save(userSave);
  }

  @Override
  public User login(String email, String password) throws UserNotFoundException {
    User user = userRepository.findByEmailAndPassword(email, password);
    if (user == null) {
      throw new UserNotFoundException("Your email or password is incorrect.");
    }
    return user;
  }

}
