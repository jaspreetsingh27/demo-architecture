package com.example.util;

public interface Constants {

  String API_PREFIX = "/api/v1";

  String DEFAULT_ERROR_MESSAGE = "Something Bad Happened ! Please try later";

}
