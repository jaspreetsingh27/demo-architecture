angular.module( 'app', [ 'ui.router', 'ngMaterial', 'ngAnimate', 'ngAria', 'ngMessages', 'templates', 'login', 'signup' ] )
  .config( function( $urlRouterProvider, $locationProvider ) {
    $locationProvider.html5Mode( true );
    $urlRouterProvider.otherwise( '/login' );
  } ).run( function() {} );