angular.module( 'login', [] ).config( function( $stateProvider ) {
  $stateProvider.state( 'login', {
    url: '/login',
    templateUrl: 'login/login.tpl',
    controller: 'LoginCtrl'
  } );
} ).controller( 'LoginCtrl', function( $scope, LoginService, $mdToast ) {
  $scope.loginCtrl = {
    user: {}
  };
  $scope.login = function( loginForm ) {
    if ( loginForm.$valid ) {
      LoginService.login( $scope.loginCtrl.user ).success( function( resp ) {
        console.log( "success", resp );
        $mdToast.show( {
          template: '<md-toast class="success-msg">' + "Successfully logged in as " + resp.data.email +
            '</md-toast>',
          hideDelay: 6000,
          position: 'top right'
        } );
      } ).error( function( resp ) {
        // Toast Message
        $mdToast.show( {
          template: '<md-toast class="error-msg">' + resp.message + '</md-toast>',
          hideDelay: 6000,
          position: 'top right'
        } );
        // 
      } );
    }
  };
} );