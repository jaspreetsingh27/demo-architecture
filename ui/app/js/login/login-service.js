angular.module( 'login' ).factory( 'LoginService', function( $http, Constants ) {
  return {
    login: function( user ) {
      return $http.post( Constants.APP_URL + 'auth/login', user );
    },
  };
} );