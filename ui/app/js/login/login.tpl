<section flex layout="row" layout-align="center center">
  <article layout="column" layout-margin layout flex="initial" style="width:400px;">
    <md-card class="card" layout-padding flex style="border-top: 1px solid #F0F0F0;">
      <form novalidate name="loginForm" autocomplete="off" ng-submit="login(loginForm)">
        <h3>Sign in</h3>
        <div layout>
          <md-input-container flex>
            <label>Email* </label>
            <input type="email" name="email" autocomplete="off" ng-model="loginCtrl.user.email" required/>
            <div ng-messages="loginForm.email.$error" ng-if="loginForm.email.$dirty || loginForm.$submitted">
              <div ng-message="required">This is required</div>
              <div ng-message="email">Invalid Email</div>
            </div>
          </md-input-container>
        </div>
        <div layout>
          <md-input-container flex>
            <label>Password* </label>
            <input type="password" name="password" autocomplete="off" class="form-control" ng-model="loginCtrl.user.password" required/>
            <div ng-messages="loginForm.password.$error" ng-if="loginForm.password.$dirty || loginForm.$submitted">
              <div ng-message="required">This is required</div>
            </div>
          </md-input-container>
        </div>
        <div flex>
          <md-button class="md-raised ladda-button" data-ui-ladda="loginCtrl.progress" data-style="expand-right" type="submit" flex>Sign in</md-button>
        </div>
      </form>
      <div flex>
        <md-button ui-sref="signup">New User? Sign up</md-button>
      </div>
    </md-card>
  </article>
</section>