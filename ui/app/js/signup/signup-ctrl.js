angular.module( 'signup', [] ).config( function( $stateProvider ) {
  $stateProvider.state( 'signup', {
    url: '/signup',
    templateUrl: 'signup/signup.tpl',
    controller: 'SignupCtrl'
  } );
} ).controller( 'SignupCtrl', function( $scope, SignupService, $mdToast ) {
  $scope.signupCtrl = {
    user: {}
  };
  $scope.signup = function( signupForm ) {
    if ( signupForm.$valid ) {
      SignupService.signup( $scope.signupCtrl.user ).success( function( resp ) {
        $state.go( 'login' );
      } ).error( function( resp ) {
        // Toast Message
        $mdToast.show( {
          template: '<md-toast class="error-msg">' + resp.message + '</md-toast>',
          hideDelay: 6000,
          position: 'top right'
        } );
        // 
      } );
    }
  };
} );