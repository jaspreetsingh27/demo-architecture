angular.module( 'signup' ).factory( 'SignupService', function( $http, Constants ) {
  return {
    signup: function( user ) {
      return $http.post( Constants.APP_URL + 'auth/signup', user );
    },
  };
} );