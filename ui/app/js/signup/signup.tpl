<section flex layout="row" layout-align="center center">
  <article layout="column" layout-margin layout flex="initial" style="width:400px;">
    <md-card class="card" layout-padding flex style="border-top: 1px solid #F0F0F0;">
      <form novalidate name="signupForm" autocomplete="off" ng-submit="signup(signupForm)">
        <h3>Sign up</h3>
        <div layout>
          <md-input-container flex>
            <label>Email* </label>
            <input type="email" name="email" autocomplete="off" ng-model="signupCtrl.user.email" required/>
            <div ng-messages="signupForm.email.$error" ng-if="signupForm.email.$dirty || signupForm.$submitted">
              <div ng-message="required">This is required</div>
              <div ng-message="email">Invalid Email</div>
            </div>
          </md-input-container>
        </div>
        <div layout>
          <md-input-container flex>
            <label>Password* </label>
            <input type="password" name="password" autocomplete="off" class="form-control" ng-model="signupCtrl.user.password" required/>
            <div ng-messages="signupForm.password.$error" ng-if="signupForm.password.$dirty || signupForm.$submitted">
              <div ng-message="required">This is required</div>
            </div>
          </md-input-container>
        </div>
        <div flex>
          <md-button class="md-raised ladda-button" data-ui-ladda="signupCtrl.progress" data-style="expand-right" type="submit">Sign in</md-button>
        </div>
      </form>
      <div flex>
        <md-button ui-sref="login">Already Registered? Login</md-button>
      </div>
    </md-card>
  </article>
</section>